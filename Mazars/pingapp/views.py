from django.http import HttpResponse
from django.shortcuts import render
from django.conf.urls import url
import requests
import json
from django.views.decorators.csrf import csrf_exempt
from rest_framework_swagger.views import get_swagger_view

@csrf_exempt
def ping(request):
	if request.method =='POST':
		try:
			kurl = request.POST['url']
			x = requests.get(kurl, verify=False)
			return HttpResponse(x.content)				
		except KeyError:
			return HttpResponse("KeyError: Missed 'url' key")
		except Exception as e:
			return HttpResponse(str(e))
							
	return HttpResponse("Ooops .... Received wrong request.")
	
@csrf_exempt	
def info(request):
	return HttpResponse('{“Receiver”: “Cisco is the best!”}')